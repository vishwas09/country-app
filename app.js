
var country_list;

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function add_card(c_data){
    var cr = Object.values(c_data.currencies);
    var str = 
    `<div class="card">
    <div class="flag">
        <img class="flag_img" src="${c_data.flags.png}" alt="flag image">
    </div>
    <div class="info_area">
        <div class="info">
            <p class="name">${c_data.name.common}</p>
            <p class="currency">Currency: ${cr[0].name}</p>
            <p class="capital">Capital: ${c_data.capital[0]}</p>
        </div>
        <div class="ref_area">
            <a href="${c_data.maps.googleMaps}" target="_blank" rel="noopener noreferrer">
                <span class="map_btn_span">Show Map</span>
            </a>
            <a href="details.html?country=${c_data.cca3}" target="_blank" rel="noopener noreferrer">
                <span class="detail_btn_span">Detail</span>
            </a>
        </div>
    </div>
    </div>`;

    $('#cards_container').append(str);

}

function init_data(){
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            country_list = JSON.parse(this.responseText);
        }
        if(this.readyState == 4 && this.status != 200){
            alert("Couldnt load data");
        }
    };
    xhr.open("GET", "https://restcountries.com/v3.1/all", false);
    xhr.send();
}

function init_page(){
    for(var i = 0; i<10; i++){
        var index = getRandomInt(0, 249);
        add_card(country_list[index]);
    }
}

function searchData(){

    var srch_input = document.getElementById("inpBox").value;
    if(srch_input === ""){
        $('#cards_container').empty();
        init_page();
        return;
    }
    else {
        var result = country_list.find(obj => {
            return (obj.name.common.toUpperCase() === srch_input.toUpperCase() || obj.cca3 === srch_input.toUpperCase() || obj.cca2 === srch_input.toUpperCase());
          });

        if(result != undefined){
            $('#cards_container').empty();
            add_card(result);
        }
        else {
            alert("Couldnt find request");
        }
    }
            
}

init_data();
init_page();
