var country_detail;
var data;

function fetch_data(url){
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            data = JSON.parse(this.responseText);
        }
        if(this.readyState == 4 && this.status != 200){
            console.log("couldnt load data\n");
        }
    };

    xhr.open("GET", url, false);
    xhr.send();
}

function load_data(){
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            country_detail = JSON.parse(this.responseText);
        }
        if(this.readyState == 4 && this.status != 200){
            alert("Couldnt load data");
        }
    };

    var url_string = window.location.href;
    var url = new URL(url_string);
    var country = url.searchParams.get("country");
    if(country == null){
        alert("Invalid URL");
        return;
    }

    xhr.open("GET", "https://restcountries.com/v3.1/alpha/" + country, false);
    xhr.send();
}

function init_details(){
    if(country_detail == null || country_detail == undefined){return;}
    
    document.getElementById("country_f_img").src = country_detail[0].flags.png;

    var native = Object.values(country_detail[0].name.nativeName);
    if(native.length > 1){
        document.getElementById("country_native").innerText = "Native Name:  " + native[1].common;
    }
    else {
        document.getElementById("country_native").innerText = "Native Name:  " + native[0].common;
    }

    document.getElementById("country_capital").innerText = "Capital:  " + country_detail[0].capital;
    document.getElementById("country_population").innerText = "Population:  " + country_detail[0].population;
    document.getElementById("country_region").innerText = "Region:  " + country_detail[0].region;
    document.getElementById("country_s_region").innerText = "Sub-Region:  " + country_detail[0].subregion;
    document.getElementById("country_area").innerText = "Area:  " + country_detail[0].area + "Km square";
    document.getElementById("country_code").innerText = "Country Code:  " + country_detail[0].idd.root + country_detail[0].idd.suffixes[0];
    
    var langs = Object.values(country_detail[0].languages);
    if(langs.length > 1){
        document.getElementById("country_languages").innerText = "Languages:  " + langs[0] + " , " + langs[1];
    }
    else{
        document.getElementById("country_languages").innerText = "Languages:  " + langs[0];
    }
    
    var currncy = Object.values(country_detail[0].currencies);
    document.getElementById("country_currency").innerText = "Currencies:  " + currncy[0].name;

    document.getElementById("country_time_zone").innerText = "Timezones:  " + country_detail[0].timezones[0];

}

function init_flags(){
    if(country_detail == null || country_detail == undefined){return;}

    var borders = country_detail[0].borders;
    if(borders != null || borders != undefined){
        for(var i = 0; i < borders.length; i++){
            url = "https://restcountries.com/v3.1/alpha/" + borders[i];
            fetch_data(url);
            var elem = document.createElement("img");
            elem.setAttribute("src", data[0].flags.png);
            elem.setAttribute("alt", "flag image");
            elem.setAttribute("class", "nb_flags");
            document.getElementById("neighbour_flags").appendChild(elem);
        }
    }
    
}

load_data();
init_details();
init_flags();